import React from 'react';

// "React.createContext()" - allows us to pass information between components without using props drilling
const UserContext = React.createContext();

// "provider" - allows other components to consume/use the context object and supple the necessary information need to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;