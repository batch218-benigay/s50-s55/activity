import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// Import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  //StrictMode - will highlight if there are potential error
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name= "Sam";
// // JSX - JavaScript XML (syntax is same with HTML) (XML - extinsible markup language)
// // With JSX we can can apply JavaScript logic with HTML elements
// const element = <h1>Hello, {name}!</h1>

// const user = {
//   firstName: 'Ayex',
//   lastName: 'Sam'
// };

// function formatName(user) {
//   return user.firstName + ' ' + user.lastName;
// };

// const element = <h1>Hello, {formatName(user)}</h1>

// root.render(element);

//index.js serves as the entry point