import CourseCard from '../components/CourseCard';
//import coursesData from '../data/coursesData';

import {useState, useEffect} from 'react';


export default function Courses() {

	// State that will be used to store the courses retrieved from the the Database
	const [ courses, setCourses ] = useState([]);

	// checks to see if the mock data was captured
	// console.log(coursesData);
	// console.log(coursesData[0]);


	//The  "map" method loops through the individual course o bject in our array and return a component for each course
	//Ever time the map method loops through the data, it creates a "CourseCard" component then passes the current element in our coursesData array using the "courseProp"
	
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} course={course}/>
	// 	)
	// })

	// Retrieves the courses from the database upon the initial render of "courses" component
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course => {
				return(
					<CourseCard key={course.id} course={course}/>
				)
			}))
		})
	}, []);
	// Props Drilling - allows us to pass information from one component to another using "props"
	// Curly braces {} are used for props to signify that we are providing /passing information
	return (
		<>
			{courses}
		</>
	)
}