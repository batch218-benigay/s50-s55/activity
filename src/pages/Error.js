import Banner from '../components/Banner';
//import { Link } from "react-router-dom";


export default function Error() {

	const data = {
		title: "Error 404 - Page not found.",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"
	}
	return(
		// <>
		// <h1>Page Not Found</h1>
		// <p>Go back to {<Link to="/">Homepage</Link>}</p>
		// </>

		<Banner data={data} />
	)
}